var {mongoose} = require('../mongoose');
const validator = require('validator');


var DetailSchema = new mongoose.Schema({
 name:{
    type: String,
    required: true,
    trim: true,
    minlength: 1

 },
 email:{
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
 },

 _creator:{
     type:mongoose.Schema.Types.ObjectId,
     required:true
 } 

});

var details = mongoose.model('detail',DetailSchema);

module.exports ={details};