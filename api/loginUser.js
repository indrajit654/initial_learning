var cookieParser = require('cookie-parser');
var {users}=require('./../model/user');
const jwt = require('jsonwebtoken'); 
var {mongoose} = require('../mongoose');
const _ = require('lodash');
login=((req, res) => {
    //  var email=req.body.email,
    //  var password=req.body.password
    // users.findByCredentials(body.email, body.password)
     var body = _.pick(req.body, ['email', 'password']);
     console.log(body);
     users.findByCredentials(body.email,body.password).then((user) => {
        console.log(user);
        user.generateAuthToken(user).then((token) => {
        res.cookie('x-auth', token);
        res.header('x-auth', token).send(user);
        res.send({message:'user logged in',token:res.token});
      });
    }).catch((e) => {
      console.log('error',e);
      res.status(400).send();
    });
});

module.exports={login}