var express = require('express');
var router = express.Router();
var adduser =require('./../api/addUser');
var fetchuser =require('./../api/loginUser');
var removeuser =require('./../api/logoutUser');
var fetchdata =require('./../api/fetch');
var adddata =require('./../api/insert');
var updatedata=require('./../api/update');
var deletedata=require('./../api/remove');
var prof=require('./../api/profile');
var {users}=require('./../model/user');
var {details}=require('./../model/details');
// var {authenticate}=require('./../middleware/Athenticate');

var authenticate = (req,res,next)=>{
    console.log('auth');
    console.log("cookie details",req.cookies);
    // console.log('res');
    var token  = req.cookies['x-auth']||req.headers['authorization']||req.headers.authorization;
       console.log("token",token);
       if (!token){
         res.render('/')
       }
       users.findByToken(token).then((user)=>{
        console.log("user",user);
        if (!user){
            console.log('user do not exist');
            window.location('/')
        }
        req.user=user;
        req.token=token;
        console.log("user",req.user);
        console.log("token",req.token);                
   
        next();
      }).catch((error)=>{
        res.status(401).send();
        console.log('error',error);
      })
    };

router.post('/signUp',adduser.signup);
router.post('/signIn',fetchuser.login);
router.post('/logOut',authenticate,removeuser.logout);
router.get('/fetch',authenticate,fetchdata.fetch);
router.post('/adddata',authenticate,adddata.add);
router.post('/update',updatedata.edit);
router.post('/delete',deletedata.del);
router.get('/profile',authenticate,prof.profile);


module.exports = router;
